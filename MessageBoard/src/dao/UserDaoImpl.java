package dao;

import model.users;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class UserDaoImpl implements usersDao {

	// 纗user戈
	public Boolean save(users user) {
		// 眔Session
		Session session = HibernateUtil.getSessionFactory().openSession();

		// 琩高琌Τuserid
		Query query = session.createQuery("from users c where c.userid=?");
		// 砞﹚琩高把计
		query.setString(0, user.getUserid());

		// 璝useridぃ碞纗
		if (query.uniqueResult() == null) {
			// 秨币ユ
			Transaction tx = session.beginTransaction();
			// 眔userン戈
			user.setUserid(user.getUserid());
			user.setPassword(user.getPassword());
			// 钡纗userン
			session.save(user);
			// 癳ユ
			tx.commit();
			return true;
		}
		return false;

	}

	// 琩高user戈
	public users userQuery(String userid,String password) {
		// 眔Session
		Session session = HibernateUtil.getSessionFactory().openSession();
		// 琩高
		Query query = session.createQuery("from users c where c.userid=? and c.password=?");
		// 砞﹚琩高把计
		query.setString(0, userid);
		query.setString(1, password);
		users u = (users) query.uniqueResult();
		return u;
	}
}