package action;

import dao.usersDao;
import model.users;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserAction {
    String userid;
    String password;
    usersDao usersDao;

	String state;
    
    

    public String getUserid() {
		return userid;
	}




	public void setUserid(String userid) {
		this.userid = userid;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public usersDao getUsersDao() {
		return usersDao;
	}




	public void setUsersDao(usersDao usersDao) {
		this.usersDao = usersDao;
	}


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String execute() throws Exception {

        // create and configure beans
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

		// retrieve configured instance
//	        建置DAO物件
        usersDao service = (usersDao) context.getBean("usersDao");

//		判斷是否輸入帳密
		if (getUserid() != null && getPassword() != null) {
			System.out.println("userid: " + getUserid());
			System.out.println("password: " + getPassword());
			users u = service.userQuery(getUserid(),getPassword());
			if (u!=null) {
//				HttpServletRequest request,HttpServletResponse response
				HttpServletRequest request = ServletActionContext.getRequest();
				HttpSession session = request.getSession();
				session.setAttribute("userid", getUserid());
//				request.setAttribute("userid", u);
				return "success";
			} else {
				System.out.println("登入失敗，請重新輸入帳密");
				setState("登入失敗，請重新輸入帳密");
			}
		}else {
			System.out.println("登入失敗，請輸入帳號與密碼");
			setState("登入失敗，請輸入帳號與密碼");
		}

		return "error";
	}
}