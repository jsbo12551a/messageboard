package action;

import dao.usersDao;
import model.users;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserSaveAction {
    String userid;
    String password;
    String state;
    usersDao usersDao;

  

    public String getUserid() {
		return userid;
	}



	public void setUserid(String userid) {
		this.userid = userid;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public usersDao getUsersDao() {
		return usersDao;
	}



	public void setUsersDao(usersDao usersDao) {
		this.usersDao = usersDao;
	}



	public String execute() throws Exception {

        // create and configure beans
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

        // retrieve configured instance
        usersDao service = (usersDao) context.getBean("usersDao");

        if(getUserid() != null && getPassword() != null) {
            System.out.println("Save Name: " + getUserid());
            System.out.println("Save Number: " + getPassword());
            users user = new users();
            user.setUserid(getUserid());
            user.setPassword(getPassword());
            Boolean u = service.save(user);
            if(u) {
            	System.out.println("成功儲存");
                return "success";
            }else {
            	System.out.println("帳號重複，請重新輸入");
                return "error";
            }
            
        }else {
            setState("請輸入帳號與密碼");
        }
        return "error";

    }
}