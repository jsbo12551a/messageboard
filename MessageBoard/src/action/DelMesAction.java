package action;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.mesDao;
import model.message;

public class DelMesAction {
	private int id;

	mesDao mesDao;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public mesDao getMesDao() {
		return mesDao;
	}
	public void setMesDao(mesDao mesDao) {
		this.mesDao = mesDao;
	}
	
	public String execute() throws Exception {
		 // create and configure beans
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

		// retrieve configured instance
//	        �ظmDAO����
        mesDao service = (mesDao) context.getBean("mesDao");
        service.mesDel(getId());


        return "success";
	}
}
