package action;

import dao.mesDao;
import dao.usersDao;
import model.message;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MesAction {
	private int id;
	private String userid;
	private String message;
	private String time;
	private String file;
	mesDao mesDao;
	private ArrayList<message> messages;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}

	
	
	public ArrayList<message> getMessages() {
		return messages;
	}
	public void setMessages(ArrayList<message> messages) {
		this.messages = messages;
	}
	
	
	
	public mesDao getMesDao() {
		return mesDao;
	}
	public void setMesDao(mesDao mesDao) {
		this.mesDao = mesDao;
	}
	
	
	public String execute() throws Exception {
		 // create and configure beans
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

		// retrieve configured instance
//	        建置DAO物件
        mesDao service = (mesDao) context.getBean("mesDao");

        ArrayList m = (ArrayList) service.mesQuery();
        
        this.messages = m;
        
			if (m!=null) {
//				 while(m.hasNext()) {
//		        	 message mes = (message) m.next();
//		        	    System.out.println(mes.getUserid() + "\t" +
//		        	    		mes.getMessage() + "\t" +
//		        	    		mes.getTime());
//		        }
				HttpServletRequest request = ServletActionContext.getRequest();
				 
				for(int i=0;i<m.size();i++) {
					message mes = (message)m.get(i);
					System.out.println(mes.getMessage());//列印從mes物件裡獲取到的值
				}
				return "success";
			} 

		return "error";
	}
	
	
}
