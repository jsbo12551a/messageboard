package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.mesDao;
import model.message;
import model.users;

public class UpMesAction {
	private int id;
	private String userid;
	private String message;
	private String time;
	private String file;
	mesDao mesDao;
	private ArrayList<message> messages;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public ArrayList<message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<message> messages) {
		this.messages = messages;
	}

	public mesDao getMesDao() {
		return mesDao;
	}

	public void setMesDao(mesDao mesDao) {
		this.mesDao = mesDao;
	}

	public String execute() throws Exception {
		// create and configure beans
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

		// retrieve configured instance
//	        �ظmDAO����
		mesDao service = (mesDao) context.getBean("mesDao");
		
		if(getMessage()!=null) {
			
            message mes = new message();
            mes.setId((long) getId());
            mes.setUserid(getUserid());
            mes.setMessage(getMessage());
			service.updata(mes);
		
		 return "success";
		}
		return "error";
	}
}
