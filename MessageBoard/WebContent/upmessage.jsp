<?xml version="1.0" encoding="BIG5" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page language="java" contentType="text/html; charset=BIG5"
	pageEncoding="BIG5"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5" />
<title>修改留言</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
</head>
<body>

<%

String id=request.getParameter("id");

String mes=request.getParameter("mes");
//out.println("接收到:"+id+" "+mes);

%>
	<div class="container-fluid h-100 w-100">
		<div class="row p-3 bg-LightGreen">
			<div class="col-6">
				<h3>MessqgeBoard</h3>
			</div>
			<div class="col-6 text-right">
				<a class="btn btn-outline-info" href="#">login</a>
			</div>
		</div>

		<div class="container">
			<form id="UpMesAction" name="UpMesAction"
				action="/MessageBoard/UpMesAction.action" method="post">
				<input type="hidden" id="id" name="id" value="${id}"/>
				<div class="form-group row p-3 mt-5">
					<!-- <label for="staticEmail" class="col-sm-2 col-form-label">userid</label> -->
					<label for="${userid}">留言作者</label> <input type="text" readonly
						class="form-control-plaintext text-info" name="userid"
						id="${userid}" value="${userid}">
				</div>
				<div class="form-group">
					<label for="message">留言內容</label>
							<textarea class="form-control" id="message" name="message"
								rows="10"><s:property value="message"/><% out.println(mes);%></textarea>
				</div>
				<!-- <div class="form-group">
					<label for="exampleFormControlFile1">Example file input</label> <input
						type="file" class="form-control-file" id="exampleFormControlFile1">
				</div> -->
				<button type="submit" class="btn btn-primary">修改留言</button>
			</form>
		</div>

	</div>



	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>
</html>