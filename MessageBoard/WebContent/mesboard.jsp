<?xml version="1.0" encoding="BIG5" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page language="java" contentType="text/html; charset=BIG5"
	pageEncoding="BIG5"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5" />
<title>messageBoard</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>


	<%
		if ((request.getSession(false).getAttribute("userid") == null)) {
	%>

	<!-- <jsp:forward page="UserAction"></jsp:forward> -->
	<div class="container h-100">
		<div class="row align-items-center h-100">
			<div class="col-6 mx-auto">
				<div class="text-center">
					<div class="col-12 pb-3">
						<h2>請先登入會員</h2>
					</div>
					<div class="col-12 p-3">
						<a href="" class="btn btn-info form-control">登入</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%
		}
	%>

	<div class="container-fluid h-100 w-100">
		<div class="row p-3 bg-LightGreen">
			<div class="col-6">
				<h3>MessqgeBoard</h3>
			</div>
			<div class="col-6 text-right">
				<a href="UserLogoutAction" class="btn btn-outline-info">登出</a>
			</div>
		</div>
		<div class="container">
			<div class="row p-3 mt-5 w-100">
				<div class="col-12 text-center">
					<a type="button" class="btn btn-outline-success form-control"
						href="addmesAction">新增留言+</a>
				</div>
			</div>

			<s:iterator value="messages">
				<div class="row p-3 mt-5">
					<div
						class="row border-bottom border-right border-left rounded pt-3 w-100">
						<div class="col-6">
							<h4 class="text-info">

								<!-- <s:property value="id" /> -->
								<s:property value="userid" />
							</h4>


						</div>
						<s:if test="userid==#session.userid">
							<div class="col-6 text-right p-2">
								<!--<form id="UpMesAction" name="UpMesAction"
									action="/MessageBoard/UpMesAction.action" method="post"
									>
									<input type="hidden" value="${id}" name="id" id="id"/> 
									<input type="hidden" value="${message}" name="message" id="message"/> 
									<a onclick="upmesForm()" href="UpMesAction.action"
										class="badge badge-success updata" id="${id}">修改</a>
									 <input type="submit" class="" value="修改"/>  
								</form>-->
								<a href="UpMesAction.action?id=${id}&mes=${message}"
										class="badge badge-success updata" id="${id}" name="${id}">修改</a>
								<a href="DelMesAction.action?id=${id}" class="badge badge-secondary deldata" id="${id}">刪除</a>
							</div>
						</s:if>
						<div class="col-12">
							<p>
								<s:property value="message" />
							</p>
						</div>
					</div>
				</div>
				<br />
			</s:iterator>
		</div>

	</div>


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script>
		function upmesForm() {
			document.getElementById("UpMesAction").submit()
		}
	</script>


</body>
</html>